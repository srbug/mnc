%exemplo 1 - aula
%A=[5 2 1;3 1 4; 1 1 3]
%b=[0;-7;-5]
%F=[2 2 3;3 3 1;0 3 4]

%exemplo 2 - exercicio aleatorio
%A=[3 2 4;1 1 2;4 3 -2];
%b=[1;2;3];

%decLU(A,b);

%exemplo 3 - TDE
%A=[1 1 0 3;2 1 -1 1;3 -1 -1 2;-1 2 3 -1];
%b=[8;7;14;-7];

%F(2,:)*b(:) = -26
%F(:,3)*b(:) -> erro
%F(:,3).*b(:) -> n erro

%Decomposição LU
function decLU(A,b)
    [m,n]=size(A); %[linhas,colunas]=size(A)
   
    %Impoem a condicao de que a matriz A deve ser quadrada
    if m ~= n %diferente: ~=
        disp('A matriz deve ser quadrada');
        fprintf('A[linha,coluna] -> A[%d,%d] não é quadrada\n',m,n);
        return
    end
    
    %Menores principais
    for k=1:n-1
        if abs(det(A(1:k,1:k)))<(10^(-10))
            fprintf('Menor principal A%d=0\n',k);
            return
        end
    end
    
    %Decomposição LU
    for i=2:n
        for j=1:n
            if i<=j %Calcular elementos de U
                S=0;
                for k=1:i-1
                    S=S+A(i,k)*A(k,j);
                end
                A(i,j)=A(i,j)-S;
            else %Calcular elementos de L
                S=0;
                for k=1:j-1
                	S=S+A(i,k)*A(k,j);
                end
                A(i,j)=(A(i,j)-S)/A(j,j);
            end
        end
    end
    disp('Na parte estritamente inferior estão os elementos de L');
    disp('Na parte diagonal e superior estão os elementos de U');
    disp(A);
    
    %Ly=b
    for i=1:n
        S=0;
        for j=1:i-1
            S=S+A(i,j)*b(j);
        end
        b(i)=b(i)-S;
    end
    disp(b);
    
    %Ux=y
    b(n)=b(n)/A(n,n);
    for i=n-1:-1:1
        S=0;
        for j=i+1:n
            S=S+A(i,j)*b(j);
        end
        b(i)=(b(i)-S)/A(i,i);
    end
    disp(b);