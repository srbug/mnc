% #########################################################################
% Exemplos de matrizes para teste (elmGaus(A,b))
%   exemplo 1 - aula
%   A = [5,2,1;3,1,4;1,1,3] e b = [7;7;13]
%   exemplo 2 - aula
%   A = [2 2 1; 3 3 -5;3 0 4] e b = [7;7;13] (esse exemplo testa pivo nulo)
%   exemplo 3 - TDE
%   A = [5,2,-2;4,5,1;3,-1,4] e b = [5;10;6]
% #########################################################################

% metodo da eliminacao de gaus
function elmGauss(A,b)
    [m,n]=size(A); %[linhas,colunas]=size(A)
    
    %Impoem a condicao de que a matriz A deve ser quadrada
    if m ~= n %diferente: ~=
        disp('A matriz deve ser quadrada');
        fprintf('A[linha,coluna] -> A[%d,%d] não é quadrada\n',m,n);
        return
    end
    
    %Lógica da eliminação de gaus - [START]
    format rat % mostra o resultado como fracao
    for k=1:n-1
        if A(k,k) == 0
            fprintf('O pivo A(%d,%d) é nulo. Verifique sua matriz\n',k,k);
            return
        end
        for i=k+1:n
            p = (A(i,k)/A(k,k));
            for j=k:n
                A(i,j)=A(i,j)-A(k,j)*p;
            end
            b(i)=b(i)-b(k)*p;
        end
        fprintf('Etapa %d do escalonamento\n',k);
        disp(A);
        disp(b);
    end
    %Lógica da eliminação de gaus - [END]
    
    %Retrosubstituicao
    x(n)=b(n)/A(n,n);
    for i=(n-1):-1:1 %for i=inicio:incremento:fim
        S=0;
        for j=(i+1):n
            S=S+A(i,j)*x(j);
        end
        x(i)=(b(i)-S)/A(i,i);
        
    end
    
    format long % mostra o resultado como decimal
    disp('A solução do sistema é:');
    x=x.'; %transposta: .'
    disp(x);
    format short % mostra o resultado no formato padrao do matlab (reseta)