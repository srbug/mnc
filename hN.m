% #########################################################################
%
% DICAS: 
%   - simpson 1/3 trabalha com uma quantidade PAR de subintervalos
%   - simpson 3/8 trabalha com uma quantidade divisivel por 3 de subintervalos
%   - trapezio trabalha com uma quantidade PAR de subintervalos
%
% hN(f,k,a,b,p)
% - h: passo
% - N: numero de subintervalos
% - f: funcao a ser integrada
% - k: Metodo/Regra escolhida
%       0 -> trapezio
%       1 -> simpson 1/3
%       2 -> simpson 3/8
% - p: precisao
%
%   exemplo 1 - aula
%       Trapezio
%           - k=0; trapezio
%           - a=0; limite inferior
%           - b=1; limite superior
%           - p=0.5*10^(-6); precisao
%       Simpson 1/3
%           - k=1; simpson 1/3
%           - a=0; limite inferior
%           - b=1; limite superior
%           - p=0.5*10^(-6); precisao
%       Simpson 3/8
%           - k=2; simpson 3/8
%           - a=0; limite inferior
%           - b=1; limite superior
%           - p=0.5*10^(-6); precisao
% #########################################################################

% Retorna o passo h e o Numero de subintervalos
function [h,N]=hN(f,k,a,b,p)
    if k==0
        fprintf('Regra do trapezio\n');
        g=diff(f,3); % derivada primeira ordem
        R=[eval(solve(g));a;b]; % os criticos
        g=diff(f,2); % derivada segunda ordem
        F=abs(eval(subs(g,R))); % as imagens da derivada de 2 ordem
        M=max([F]); % as imagens deles (criticos) na derivada de 2 ordem (modulo)
        h=(12*p/((b-a)*M))^0.5; % h
    elseif k==1
        fprintf('Regra 1/3 de simpson\n');
        g=diff(f,5);
        R=[eval(solve(g));a;b];
        g=diff(f,4);
        F=abs(eval(subs(g,R)));
        M=max([F]);
        h=(180*p/((b-a)*M))^0.25;
    elseif k==2
        fprintf('Regra 3/8 de simpson\n');
        g=diff(f,5);
        R=[eval(solve(g));a;b];
        g=diff(f,4);
        F=abs(eval(subs(g,R)));
        M=max([F]);
        h=(80*p/((b-a)*M))^0.25;
    else 
        fprintf('Opcao invalida\n');
        return
    end
    N=(b-a)/h;
    fprintf('\nN=%f\n',N);
    fprintf('h=%f\n',h);
    