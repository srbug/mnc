% vetor
u=[4,5,6];
v=[10 11  12];
t=u+v;

% vetor coluna
w=[5;-2;4]; % ; quebra a linha do vetor
A=[5,8;20,21]; % A(1,2) -> 8

%pegando posição de vetor
r=[pi,2.4 8]; % r(1) -> 3.1416

%potencia 
a=2;
b=5;
c=a^b;

