% #########################################################################
% Exemplos de teste rk4(f,x0,y0,xN,N) - Runge Kutta
%   exemplo 1 - aula
%       syms x y;
%       f=-y+x+2; % equacao diferencial na forma y'=f(x,y)
%       x0=0; % chute inicial do PVI
%       y0=2; % chute inicial do PVI (y(x0) = y0)
%       xN=0.3; % último valor a ser aproximado 
%       N=3; % total de passos a ser realizados
%   exemplo 2 
%       a) TDE (1/3)-(exp(-3*t))/3 ->  (1/3)-(exp(-3*1))/3 = 0.3167
%       	syms x y;
%       	f=1-3*y; % equacao diferencial na forma y'=f(x,y) -> t - 3y
%           x0=0; % chute inicial do PVI
%           y0=0; % chute inicial do PVI (y(x0) = y0)
%           xN=1; % último valor a ser aproximado 
%           N=2; % total de passos a ser realizados
%       b) TDE (1/3)-(exp(-3*t))/3 ->  (1/3)-(exp(-3*1))/3 = 0.3167
%           syms x y;
%           f=1-3*y; % equacao diferencial na forma y'=f(x,y) -> t - 3y
%           x0=0; % chute inicial do PVI
%           y0=0; % chute inicial do PVI (y(x0) = y0)
%           xN=1; % último valor a ser aproximado 
%           N=4; % total de passos a ser realizados
%   exemplo 3 - TDE
%       5) 
%           funcao analitica -> ((g*m)/k)-((g*m)/k)*exp(-(k/m)*t)
%               g=9.8;
%               m=0.2;
%               k=0.1;
%               t=x(n); -> calculado a cada iteracao
%           PVI: 
%               syms x y; 
%               f=9.8-0.5*y; 
%               x0=0; 
%               y0=0; 
%               xN=5; 
%               N=50;
% #########################################################################
% 
%rk4 - Runge Kutta de ordem 4
function rk4(f,x0,y0,xN,N)
    syms x y;
    h=(xN-x0)/N;
    resultado=zeros(N+1,2);
    resultado(1,:)=[x0,y0];
    fprintf("\ny\': %s\n", f);
    fprintf('x0: %f\n', x0);
    fprintf('y0: %f\n', y0);
    fprintf('xN: %f\n', xN);
    fprintf('N: %f\n', N);
    fprintf('h: %f\n\n', h);
    for i=1:N
        fprintf('Para n=%d x%d=%f e y%d=%f\n',(i-1),(i-1),x0,(i-1),y0);
        % K1 --
        expK1 = replace(string(f),"x",string(x0));
        expK1 = replace(expK1,"y",string(y0));
        K1=eval(subs(f,{x,y},{x0,y0}));
        fprintf('K1 = f(%f,%f) = %s = %f\n',x0,y0,expK1,K1);
        % K2 --
        expK2 = replace(string(f),"x",string(x0+h/2));
        expK2 = replace(expK2,"y",string(y0+h*K1/2));
        K2=eval(subs(f,{x,y},{x0+h/2,y0+h*K1/2}));
        fprintf('K2 = f(%f,%f) = %s = %f\n',x0+h/2,y0+h*K1/2,expK2,K2);
        % K3 --
        expK3 = replace(string(f),"x",string(x0+h/2));
        expK3 = replace(expK3,"y",string(y0+h*K2/2));
        K3=eval(subs(f,{x,y},{x0+h/2,y0+h*K2/2}));
        fprintf('K3 = f(%f,%f) = %s = %f\n',x0+h/2,y0+h*K2/2,expK3,K3);
        % K4 --
        expK4 = replace(string(f),"x",string(x0+h));
        expK4 = replace(expK4,"y",string(y0+h*K3));
        K4=eval(subs(f,{x,y},{x0+h,y0+h*K3}));
        fprintf('K4 = f(%f,%f) = %s = %f\n',x0+h,y0+h*K3,expK4,K4);
        % y(n) --
        fprintf('y%d = %f+%f*(%f+2*%f+2*%f+%f)/6 = ',i,y0,h,K1,K2,K3,K4);
        y0=y0+h*(K1+2*K2+2*K3+K4)/6;
        fprintf('%f\n',y0);
        x0=x0+h;
        resultado(i+1,:)=[x0,y0];
        fprintf('#####\n');
    end
    disp('Solucao obtida na forma (xn,yn) - TABELA');
    fprintf('(\t   Xn\t  |\t     Yn\t\t )\n');
    for i=1:N+1
        fprintf('(%.10f |\t%.10f )\n',resultado(i,1),resultado(i,2));
    end
    plot(resultado(:,1),resultado(:,2),'*r')
    grid on