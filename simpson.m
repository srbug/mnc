% #########################################################################
% Exemplos de teste -> simpson(f,a,b,n)
%   DICA: n = b/h
%   
%   exemplo 1 - teste com a regra 1/3 de Simpson
%       - syms('x');
%       - f=exp(x)*cos(x); -> f(x)
%       - a=0; -> limite inferior
%       - b=1.2; -> limite superior
%       - n=6; -> numero de sub intervalos
%   exemplo 2 - teste com a regra 3/8 de Simpson
%       - syms('x');
%       - f=exp(x)*cos(x); -> f(x)
%       - a=0; -> limite inferior
%       - b=1.2; -> limite superior
%       - n=9; -> numero de sub intervalos
%   exemplo 3 - teste com a forma mista -> simpson(1/3 + 3/8)
%       - syms('x');
%       - f=exp(x)*cos(x); -> f(x)
%       - a=0; -> limite inferior
%       - b=1.2; -> limite superior
%       - n=7; -> numero de sub intervalos
% #########################################################################

%Regras de Simpson
function [I]=simpson(f,a,b,n)
        fprintf('Sera calculado pela regra 3/8 de Simpson\n');
        h=(b-a)/n;
        fprintf('\nParametros de entrada--\n');
        fprintf('a=%f\n', a);
        fprintf('b=%f\n', b);
        fprintf('f(x)=%s\n', f);
        fprintf('numero de subintervalos=%f\n', n);
        fprintf('h=%f\n', h);
        fprintf('\n\n');
        I=eval(subs(f,a))+eval(subs(f,b)); %f(0)+f(n) soma das 2 pontas
        for i=1:n-1
            if rem(i,3)==0
                I=I+2*eval(subs(f,(a+(i*h))));
            else
                I=I+3*eval(subs(f,(a+(i*h))));
            end
        end
        I=I*h*3/8;
        fprintf('A aproximacao da integral de %s\n',f);
        fprintf('de %.4f ate %.4f com %d subintervalos é\n%.20f\n',a,b,n,I);