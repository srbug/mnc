% #########################################################################
% https://pt.symbolab.com/solver/simplify-calculator/
%
% DICA: P2(x)=f0*l0 + f1*l1 + f2*l2 + ... + f(n)*l(n)
%
% lagrange(x,y);
%   exemplo 1 - aula
%   x = [-1 0 3]; y = [15 8 -1];
%   exemplo 2 - TDE
%   x = [1 2 3]; y = [1.5708 1.5719 1.5739];
% #########################################################################
function lagrange(t,f)
    x = sym('x');
    
    %verifica se existe um y, para cada f(x)
    if length(t) ~= length(f)
        disp('Quantidade de valores em x e y nao confere!');
        return;
    end
    n = length(t);
    
    Pn = 0;
    for k = 1:n
        Lk = 1;
        flag = true;
        up = '';
        down = '';
        for i = 1:n
            if k ~= i
                if flag
                    up = ['(' 'x - (' num2str(t(i),'%1.0f') '))'];
                    down = ['(' num2str(t(k),'%1.0f') ' - (' num2str(t(i),'%1.0f') '))'];
                    flag = false;
                else
                    up = [up '*' '(' 'x - (' num2str(t(i),'%1.0f') '))'];
                    down = [down '*(' num2str(t(k),'%1.0f') ' - (' num2str(t(i),'%1.0f') '))'];
                end
                Lk = Lk * (x - t(i))/(t(k) - t(i));
            end
        end
        fprintf('L_%d(x) = ',k-1);
        exp = ['(' up ')' '/' '(' down ')'];
        fprintf('%s simplificando -> ', exp);
        expSimplify =simplify(eval(exp));
        fprintf('%s ', expSimplify);
        up = '';
        down = '';
        Pn = Pn + (f(k) * Lk);
        fprintf('\nf_%d(x)*L_%d(x) -> (%f*%s)+',(k-1),(k-1),f(k),expSimplify);
        fprintf('\n-----\n');
    end
    Pn=simplify(Pn);
    fprintf('P_%d(x)=%s\n' ,n-1,Pn);
    %Gráfico
    eixox=linspace(t(1)-1,t(n)+1); %   linspace(a,b): gera 100 valores entre a e b
    eixoy=subs(Pn,eixox);
    plot(t,f,'b*',eixox,eixoy,'k');        %COR FORMATO TIPO_DE_LINHA
    grid on;
    title('Pn(x)');
    xlabel('x');
    ylabel('y');
    