%Polinômio Interpolador de Lagrange
function PnLagrange(t,f)
x=sym('x')
if length(t)~=length(f)
    disp('Quantidade de valores em x e y não confere!')
    return
end
n=length(t);
Pn=0;
for k=1:n
    Lk=1;
    for i=1:n
        if k~=i
            Lk=Lk*(x-t(i))/(t(k)-t(i));
        end
    end
    fprintf('L_%d(x)=%s\n',k-1,Lk)
    Pn=Pn+f(k)*Lk;
end
Pn=simplify(Pn);
fprintf('P_%d(x)=%s\n' ,n-1,Pn)
%Gráfico
eixox=linspace(t(1)-1,t(n)+1); %   linspace(a,b): gera 100 valores entre a e b
eixoy=subs(Pn,eixox);
plot(t,f,'b*',eixox,eixoy,'k')        %COR FORMATO TIPO_DE_LINHA
grid on
title('Pn(x)')
xlabel('x')
ylabel('y')

% (1.5708*((((2.5)^2)-(5*((2.5)))+6)/2)) + (1.5719*((((2.5)^2)-(4*(2.5))+3)/-1)) + (1.5739*((((2.5)^2)-(3*(2.5))+2)/2))