% #########################################################################
% Exemplos de teste -> trapezio(f,a,b,n)
%   DICA: n = b/h
%   
%   exemplo 27 - aula
%       - syms('x');
%       - f=exp(x)*cos(x); -> f(x)
%       - a=0; -> limite inferior
%       - b=1.2; -> limite superior
%       - n=6; -> numero de sub intervalos
%   exemplo - TDE
%       - syms('x');
%       - f=sqrt(1+((8*x)-3)^2) -> f(x)
%       - a=0; -> limite inferior
%       - b=1; -> limite superior
%       - n=2|n=4|n=10; -> numero de sub intervalos
%   exemplo - TDE 3
%       - syms('x');
%       - f=(1/(sqrt(2*pi)))*exp(-x^2/2);
%       - a=-1; -> limite inferior
%       - b=1; -> limite superior
%       - n=200000; -> numero de sub intervalos
% #########################################################################

% (1/(6.823185307)^1/2)*exp(-x^2/2) -> funcao do cara 

%Regra do Trapezio
function [I]=trapezio_mod(f,a,b,n)
    fprintf('Regra do Trapezio --- \n');
    h=(b-a)/n;
    fprintf('\nParametros de entrada--\n');
    fprintf('a=%f\n', a);
    fprintf('b=%f\n', b);
    fprintf('f(x)=%s\n', f);
    fprintf('numero de subintervalos=%f\n', n);
    fprintf('h=%f\n', h);
    fprintf('\n\n');
    fprintf('\n\nh/2=%f',h/2);
    fprintf('\n%s\n',subs(f,a));
    I=eval(subs(f,a))+eval(subs(f,b)); %f(0)+f(n) soma das 2 pontas
    for i=1:n-1
        %fprintf('%s=%f\n',subs(f,(a+(i*h))),eval(subs(f,(a+(i*h)))));
        I=I+2*eval(subs(f,(a+(i*h))));
    end
    fprintf('%f\n\n',eval(subs(f,b)));
    I=I*h/2;
    fprintf('A aproximacao da integral de %s\n',f);
    fprintf('de %.4f ate %.4f com %d subintervalos é\n%.20f\n',a,b,n,I);
    