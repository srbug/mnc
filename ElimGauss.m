%Método de eliminação de Gauss
function ElimGauss(A,b)
%Verificação da condição
[m,n]=size(A); %[linhas,colunas]=size(A)
if m ~= n  %diferente: ~=
    disp('A matriz deve ser quadrada')
    fprinf('%d linha(s) X %d coluna(s)\n',m,n)
    return
end
%Eliminação de Gauss
format rat %mostra os resultados na forma fracionária
for k=1:n-1 %for 
    if A(k,k)==0
        fprintf('Pivo A(%d,%d) nulo. Verifique a matriz\n',k,k)
        return
    end
    for i=k+1:n
        p=A(i,k)/A(k,k);
        for j=k:n
            A(i,j)=A(i,j)-A(k,j)*p;
        end
        b(i)=b(i)-b(k)*p;
    end
    fprintf('Etapa %d do escalonamento\n',k);
    A;
    b;
end
%RETROSUBSTITUIÇÃO
x(n)=b(n)/A(n,n);
for i=n-1:-1:1 %for i=INICIO:INCREMENTO:FIM
    S=0;
    for j=i+1:n
        S=S+A(i,j)*x(j);
    end
    x(i)=(b(i)-S)/A(i,i);
end
disp('A solução do sistema é:')
x=x.';  %transposta:  .'
format long %formata os resultados no formato X.XXXXXXXXXXXXXXX
x;
format short  %formato padrão do matlab