% #########################################################################
% Exemplos de matrizes para teste (elmGaus(A,b))
%   exemplo 1 - aula
%   A = [10 2 1;1 5 1;2 3 10]; b = [7;-8;6]; e = 10^-2;
%   exemplo 2 - TDE
%   A=[5 1 0 3;2 5 -1 1;3 -1 -7 2;-1 2 3 10]; b=[1;-5;-5;-16]; e = 10^-2;
%   exemplo 3 - aleatorio
%   A = [3 1 -1;1 4 1;1 2 -5]; b = [3;6;-6]; e = 10^-2;
% #########################################################################

%Jacobi-Richardson
function jacobi_mod(A,b,e)
    [m,n]=size(A); %[linhas,colunas]=size(A)
    if m ~= n %diferente: ~=
        disp('A matriz deve ser quadrada');
        return
    end
    
    disp('A=');
    disp(A); 
    disp('b=');
    disp(b);
    
    %Verificando elementos da diagonal principal
    for i=1:n
        if abs(A(i,i))<10^(-10)
            fprintf('Elemento A(%d,%d)=0',i,i)
            disp('Elemento da diagonal principal não pode ser zero!')
        end
    end
    
    %Matriz A* e b*
    b=b./diag(A);
    A=A./diag(A);
    disp('A*=')
    disp(A)
    disp('b=')
    disp(b)
    
    %########## analisando os critério de convergência ##########
    
    NC=false;
     
    %EDD - LINHA
    % Testa a norma linha - com A*
    fprintf('-- criterio das linhas --\n');
    for i=1:n
        fprintf('linha %d --> ',i);
        for down=1:i-1
            if (down==(i-1))
                fprintf('|%f|',A(i,down));
            else
                fprintf('|%f|',A(i,down));
            end
        end
        for up=i+1:n
            if (up==n)
                fprintf('|%f|',A(i,up));
            else
                fprintf('|%f|',A(i,up));
            end
        end
        S=sum(abs(A(i,1:i-1)))+sum(abs(A(i,i+1:n)));
        if S>=1
            fprintf('A linha %d não é EDD\n',i)
            NC=true;
            break;
        end
        fprintf(' = %f < 1 linha verificada\n',S);
    end
 
    %EDD - COLUNA
    % Testa a norma coluna - com A*
    if NC
        fprintf('\nEstou simulando que a norma linha falhou para executar pela norma coluna!\n')
        disp('Testa a matriz A pela norma coluna')
        disp(A);
        A=A'; %Transposta da matriz A*
        disp('Transposta da matriz A*')
        disp(A)
        for i=1:n
            fprintf('coluna %d --> ',i);
            for down=1:i-1
                 if (down==i-1)
                    fprintf('|%f|',A(i,down));
                 else
                    fprintf('|%f|',A(i,down));
                 end
            end
            for up=i+1:n
                 if (up==n)
                    fprintf('|%f|',A(i,up));
                 else
                    fprintf('|%f|',A(i,up));
                 end
            end
            S=sum(abs(A(i,1:i-1)))+sum(abs(A(i,i+1:n)));
            if S>=1
                fprintf('A coluna %d não atende a norma coluna\n',i)
                disp('Convergência não garantida!')
                return
            end
            fprintf(' = %f < 1 coluna verificada\n',S);
        end
        A=A';
        disp('Matriz A verificada pela norma coluna!!! Convergência garantida!');
         fprintf('\n');
    else
        disp('A matriz é EDD. Convergência garantida!');
        fprintf('\n');
    end
    
    disp('Isolar a diagonal de cada equação');
    for i=1:n
        fprintf('x%d\t=\t',i);
        for j=1:n
            if j ~= i
                fprintf('%.4fx%d\t', -A(i,j),j);
            else
                fprintf('\t\t');
            end
        end
        if b(i) > 0
            fprintf('+%.4f%d\t', b(i));
        else
            fprintf('%.4f%d\t', b(i));
        end
        fprintf('\n');
    end
    
    fprintf('\n\n');
    disp('########## Jacobi-Richardson ##########');

    %########## Jacobi-Richardson ##########
     k = 0;
     erro = 100;
     x = b;
     while (k < 1000) && (erro > e)
        xant = x; % Xk+1 = Xk
        fprintf('---\nPara k=%d, x(%d)=(',k,k);
        for idx=1:n
            if idx==n
                fprintf('%f',x(idx));
            else
                fprintf('%f ,',x(idx));
            end
        end
        fprintf(')\n');
        for i=1:n
            x(i) = b(i) - A(i,1:i-1) * xant(1:i-1) - A(i,i+1:n) * xant(i+1:n);
            for down=1:i-1
                if (down==i-1)
                    if(-A(i,down) >= 0)
                        fprintf('+%f*(%f)',-A(i,down),xant(down));
                    else
                        fprintf('%f*(%f)',-A(i,down),xant(down));
                    end
                    if i==n
                        if b(i) > 0
                            fprintf('+%f',b(i));
                        else
                            fprintf('%f',b(i));
                        end
                        fprintf(' = %f',x(i));
                    end
                else
                    fprintf('%f*(%f)',-A(i,down),xant(down));
                end
            end
            for up=i+1:n
                if (up==n)
                    if (-A(i,up) >= 0)
                        fprintf('+%f*(%f)',-A(i,up),xant(up));
                    else
                        fprintf('%f*(%f)',-A(i,up),xant(up));
                    end                    
                    if b(i) > 0
                        fprintf('+%f',b(i));
                    else
                        fprintf('%f',b(i));
                    end
                    fprintf(' = %f',x(i));
                else
                    if (-A(i,up) >= 0)
                        fprintf('+%f*(%f)',-A(i,up),xant(up));
                    else
                        fprintf('%f*(%f)',-A(i,up),xant(up));
                    end
               end
            end
            fprintf('\n');
        end
        
        fprintf('CP = ||(');
        for idx=1:n
            if idx==n
                fprintf('%f',x(idx));
            else
                fprintf('%f,',x(idx));
            end
        end
        fprintf(')-(');
        for idx=1:n
            if idx==n
                fprintf('%f',xant(idx));
            else
                fprintf('%f,',xant(idx));
            end
        end
        fprintf(')||/');
        fprintf('||(');
        for idx=1:n
            if idx==n
                fprintf('%f',x(idx));
            else
                fprintf('%f,',x(idx));
            end
        end
        fprintf('||)\n');
        erro = max(abs(x-xant))/max(abs(x));
        fprintf('CP = (%f/%f) = %f\n\n', max(abs(x-xant)),max(abs(x)),erro);
        k = k + 1;
     end