% #########################################################################
% Exemplos de matrizes para teste (elmGauss(A,b))
%   exemplo 1 - aula
%   A = [5 1 1;3 4 1;3 3 6]; b = [5;6;0]; e = 10^-2;
%   exemplo 2 - TDE
%   A=[5 2 -2;4 5 1;3 -1 4]; b=[5;10;6]; e = 10^-2;
%   A=[5 1 0 3;2 5 -1 1;3 -1 -7 2;-1 2 3 10]; b=[1;-5;-5;-16]; e = 10^-2;
%   exemplo 3 - TDE
%   A=[-1 0 0 0.7071 1 0 0 0;0 -1 0 0.7071 0 0 0 0;0 0 -1 0 0 0 0.5 0;0 0 0
%   -0.7071 0 -1 -0.5 0;0 0 0 0 -1 0 0 1;0 0 0 0 0 1 0 0;0 0 0 -0.7071 0 0
%   0.8660 0;0 0 0 0 0 0 -0.8660 -1]; b=[0;0;0;0;0;10000;0;0]; e = 10^-8;
% #########################################################################

function gaussSeidel_mod(A,b,e)
    [m,n]=size(A); %[linhas,colunas]=size(A)
    if m ~= n %diferente: ~=
        disp('A matriz deve ser quadrada');
        return
    end
    
    disp('A=');
    disp(A); 
    disp('b=');
    disp(b);
    
    %Verificando elementos da diagonal principal
    for i=1:n
        if abs(A(i,i))<10^(-10)
            fprintf('Elemento A(%d,%d)=0',i,i)
            disp('Elemento da diagonal principal não pode ser zero!')
        end
    end
    
    %Matriz A* e b*
    b=b./diag(A);
    A=A./diag(A);
    disp('A*=')
    disp(A)
    disp('b=')
    disp(b)
    
    %########## analisando os critério de convergência ##########
    
    CS=false; %Variável lógica para controlar o criterio de Sassenfeld
     
    %Criterio das linhas (aplicado em A*) <==> EDD (aplicado em A) ; % alterado
    fprintf('-- criterio das linhas --\n');
    for i=1:n
        fprintf('linha %d --> ',i);
        for down=1:i-1
            if (down==(i-1))
                fprintf('|%f|',A(i,down));
            else
                fprintf('|%f|',A(i,down));
            end
        end
        for up=i+1:n
            if (up==n)
                fprintf('|%f|',A(i,up));
            else
                fprintf('|%f|',A(i,up));
            end
        end
        S=sum(abs(A(i,1:i-1)))+sum(abs(A(i,i+1:n)));
        if S>=1
            fprintf('\n--> A linha %d não é EDD\n\n\n',i)
            CS=true;
            break;
        end
        fprintf(' = %f < 1 linha verificada\n',S);
    end
    
    % Testa o criterio de Sassenfeld
    if CS
        disp('Aplicar criterio de Sassenfeld na matriz A');
        disp(A);
        beta(n) = 0; 
        for i=1:n
            fprintf('linha %d --> ',i);
            for down=1:i-1
                 if (down==i-1)
                    fprintf('(|%f|*|%f|)',beta(down),A(i,down));
                 else
                    fprintf('(|%f|*|%f|)',beta(down),A(i,down));
                 end
            end
            for up=i+1:n
                 if (up==n)
                    fprintf('|%f|',A(i,up));
                 else
                    fprintf('|%f|',A(i,up));
                 end
            end
            beta(i)=sum(abs(beta(1:i-1).*A(i,1:i-1)))+sum(abs(A(i,i+1:n)));
            if max(beta)>=1
                fprintf('Linha %d não atende o criterio de Sassenfeld\n',i);
                disp('Convergência não garantida!')
                return
            end
            fprintf(' = %f < 1 linha verificada\n', beta(i));
        end
        disp('Criterio de Sassenfeld verificado. Convergência garantida!');
        fprintf('\n');
    else
        disp('A matriz é EDD. Convergência garantida!');
        fprintf('\n');
    end
    
    disp('Isolar a diagonal de cada equação');
    for i=1:n
        fprintf('x%d\t=\t',i);
        for j=1:n
            if j ~= i
                fprintf('%.4fx%d\t', -A(i,j),j);
            else
                fprintf('\t\t');
            end
        end
        if b(i) > 0
            fprintf('+%.4f%d\t', b(i));
        else
            fprintf('%.4f%d\t', b(i));
        end
        fprintf('\n');
    end
    
    fprintf('\n\n');
    disp('########## Gauss-Seidel ##########');

    %########## Gauss-Seidel ########## 
     k = 0;
     erro = 100;
     x(n,1)=0;
     while (k < 1000) && (erro > e)
        xant = x; % Xk+1 = Xk
        fprintf('---\nPara k=%d, x(%d)=(',k,k);
        for idx=1:n
            if idx==n
                fprintf('%f',x(idx));
            else
                fprintf('%f ,',x(idx));
            end
        end
        fprintf(')\n');
        for i=1:n
            x(i) = b(i) - A(i,1:i-1) * x(1:i-1) - A(i,i+1:n) * xant(i+1:n);
            
            
            for down=1:i-1
                if (down==i-1)
                    if (-A(i,down) >= 0)
                        fprintf('+%f*(%f)',-A(i,down),x(down));
                    else
                        fprintf('%f*(%f)',-A(i,down),x(down));
                    end
                    if i==n
                        if b(i) >= 0
                            fprintf('+%f',b(i));
                        else
                            fprintf('%f',b(i));
                        end
                        fprintf(' = %f',x(i));
                    end
                else
                    fprintf('%f*(%f)',-A(i,down),x(down));
                end
            end
            for up=i+1:n
                if (up==n)
                    if (-A(i,up) >= 0)
                        fprintf('+%f*(%f)',-A(i,up),xant(up));
                    else
                        fprintf('%f*(%f)',-A(i,up),xant(up));
                    end
                    if b(i) > 0
                        fprintf('+%f',b(i));
                    else
                        fprintf('%f',b(i));
                    end
                    fprintf(' = %f',x(i));
                else
                    if (-A(i,up) >= 0)
                        fprintf('+%f*(%f)',-A(i,up),xant(up));
                    else
                        fprintf('%f*(%f)',-A(i,up),xant(up));
                    end
               end
            end
            fprintf('\n');
        end
        fprintf('CP = ||(');
        for idx=1:n
            if idx==n
                fprintf('%f',x(idx));
            else
                fprintf('%f,',x(idx));
            end
        end
        fprintf(')-(');
        for idx=1:n
            if idx==n
                fprintf('%f',xant(idx));
            else
                fprintf('%f,',xant(idx));
            end
        end
        fprintf(')||/');
        fprintf('||(');
        for idx=1:n
            if idx==n
                fprintf('%f',x(idx));
            else
                fprintf('%f,',x(idx));
            end
        end
        fprintf('||)\n');
        erro = max(abs(x-xant))/max(abs(x));
        fprintf('CP = (%f/%f) = %f\n\n', max(abs(x-xant)),max(abs(x)),erro);
        k = k + 1;
     end