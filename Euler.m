% #########################################################################
% Exemplos de teste Euler(f,x0,y0,xN,N)
%   exemplo 1 - aula
%       syms x y;
%       f=-y+x+2; % equacao diferencial na forma y'=f(x,y)
%       x0=0; % chute inicial do PVI
%       y0=2; % chute inicial do PVI (y(x0) = y0)
%       xN=0.3; % último valor a ser aproximado 
%       N=3; % total de passos a ser realizados
%   exemplo 2 
%       a) TDE (1/3)-(exp(-3*t))/3 ->  (1/3)-(exp(-3*1))/3 = 0.3167
%       	syms x y;
%           f=1-3*y; % equacao diferencial na forma y'=f(x,y) -> t - 3y
%           x0=0; % chute inicial do PVI
%           y0=0; % chute inicial do PVI (y(x0) = y0)
%           xN=1; % último valor a ser aproximado 
%           N=2; % total de passos a ser realizados
%       b) TDE (1/3)-(exp(-3*t))/3 ->  (1/3)-(exp(-3*1))/3 = 0.3167
%       	syms x y;
%           f=1-3*y; % equacao diferencial na forma y'=f(x,y) -> t - 3y
%           x0=0; % chute inicial do PVI
%           y0=0; % chute inicial do PVI (y(x0) = y0)
%           xN=1; % último valor a ser aproximado 
%           N=4; % total de passos a ser realizados
% #########################################################################

%Euler - Método de Euler para EDOs
function Euler(f,x0,y0,xN,N)
    syms x y;
    h=(xN-x0)/N;
    resultado=zeros(N+1,2);
    resultado(1,:)=[x0,y0];
    fprintf("\ny\': %s\n", f);
    fprintf('x0: %f\n', x0);
    fprintf('y0: %f\n', y0);
    fprintf('xN: %f\n', xN);
    fprintf('N: %f\n', N);
    fprintf('h: %f\n\n', h);
    for i=1:N
        expression = replace(string(f),"x",string(x0));
        expression = replace(expression,"y",string(y0));
        fv = eval(subs(f,{x,y},{x0,y0}));
        fprintf('Para n=%d x%d=%f e y%d=%f\n',(i-1),(i-1),x0,(i-1),y0);
        fprintf('f%d(%f,%f) = %s = %f\n',(i-1),x0,y0,expression,fv);
        fprintf('y%d = y%d + h * f%d -> %f + %f * %f = ',i,(i-1),(i-1),y0,h,fv);
        y0=y0+h*eval(subs(f,{x,y},{x0,y0}));
        fprintf('%f\n',y0);
        x0=x0+h;
        resultado(i+1,:)=[x0,y0];
        fprintf('########\n\n');
    end
    disp('Solucao obtida na forma (Xn,Yn) - TABELA');
    fprintf('(\tXn\t\t Yn\t  )\n');
    for i=1:N+1
        fprintf('(%.4f | %4f)\n',resultado(i,1),resultado(i,2));
    end
    plot(resultado(:,1),resultado(:,2),'*r');
    grid on;