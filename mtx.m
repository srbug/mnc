% #########################################################################
% Exemplos de multiplicação de matrizes - mtx(A,B)
%   exemplo 1
%   A=[1 2;3 4]; B=[-1 3; 4 2];
%   exemplo 2
%   A=[2 3;0 1;-1 4]; B=[1 2 3;-2 0 4];
%   exemplo 3 - https://www.youtube.com/watch?v=eCmv6v53V88
%   A=[2 3 1;0 1 2]; B=[2 0;1 -1;3 5]; -> A(2x3) * B(3x2) = C(2x2)
%   A=[2;3]; B=[1 2 3];
% #########################################################################
function [C]=mtx(A,B)
    % regra 1)
    % Para multiplicar 2 matrizes, o numero de colunas da primeira deve ser
    % igual ao numero de linhas da segunda
    
    [~,columnA]=size(A); %[linhas,colunas]=size(A)
    [rowB,~]=size(B);    %[linhas,colunas]=size(B)
    
    % Numero de colunas de A deve ser igual ao numero de linhas de B
    if columnA ~= rowB
        fprintf('O numero de colunas de A deve ser igual ao numero de linhas de B\n');
        fprintf('A[_,%d] B[%d,_] \n',columnA, rowB);
        return
    end
    
    % o resultado será uma matriz, com o numero de linhas igual o da
    % primeira e o numero de colunas igual o da segunda
    % A(mxn) * B(nxp) = C(mxp)
    C = A*B;
    