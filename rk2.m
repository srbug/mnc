% #########################################################################
% Exemplos de teste rk2(f,x0,y0,xN,N) - Runge Kutta
%   exemplo 1 - aula
%       syms x y;
%       f=-y+x+2; % equacao diferencial na forma y'=f(x,y)
%       x0=0; % chute inicial do PVI
%       y0=2; % chute inicial do PVI (y(x0) = y0)
%       xN=0.3; % último valor a ser aproximado 
%       N=3; % total de passos a ser realizados
%   exemplo 2 
%       a) TDE (1/3)-(exp(-3*t))/3 ->  (1/3)-(exp(-3*1))/3 = 0.3167
%           syms x y;
%           f=1-3*y; % equacao diferencial na forma y'=f(x,y) -> t - 3y
%           x0=0; % chute inicial do PVI
%           y0=0; % chute inicial do PVI (y(x0) = y0)
%           xN=1; % último valor a ser aproximado 
%           N=2; % total de passos a ser realizados
%       b) TDE (1/3)-(exp(-3*t))/3 ->  (1/3)-(exp(-3*1))/3 = 0.3167
%           syms x y;
%           f=1-3*y; % equacao diferencial na forma y'=f(x,y) -> t - 3y
%           x0=0; % chute inicial do PVI
%           y0=0; % chute inicial do PVI (y(x0) = y0)
%           xN=1; % último valor a ser aproximado 
%           N=2; % total de passos a ser realizados
% #########################################################################

%rk2 - Runge Kutta de ordem 2
function rk2(f,x0,y0,xN,N)
    syms x y;
    h=(xN-x0)/N;
    fprintf('Passo utilizado h=%f\n\n',h);
    resultado=zeros(N+1,2);
    resultado(1,:)=[x0,y0];
    fprintf("y\': %s\n", f);
    fprintf('x0: %f\n', x0);
    fprintf('y0: %f\n', y0);
    fprintf('xN: %f\n', xN);
    fprintf('N: %f\n', N);
    fprintf('h: %f\n\n', h);
    for i=1:N
        fprintf('Para n=%d x%d=%f e y%d=%f\n',(i-1),(i-1),x0,(i-1),y0);
        % K1 --
        expK1 = replace(string(f),"x",string(x0));
        expK1 = replace(expK1,"y",string(y0));
        K1=eval(subs(f,{x,y},{x0,y0}));
        fprintf('K1 = f(%f,%f) = %s = %f\n',x0,y0,expK1,K1);
        % K2 --
        expK2 = replace(string(f),"x",string(x0+h/2));
        expK2 = replace(expK2,"y",string(y0+h*K1/2));
        K2=eval(subs(f,{x,y},{x0+h/2,y0+h*K1/2}));
        fprintf('K2 = f(%f,%f) = %s = %f\n',x0+h/2,y0+h*K1/2,expK2,K2);
        % y(n) --
        fprintf('y%d = %f+%f*%f = ',i,y0,h,K2);
        y0=y0+h*K2;
        fprintf('%f\n',y0);
        x0=x0+h;
        resultado(i+1,:)=[x0,y0];
        fprintf('#####\n');
    end
    disp('Solucao obtida na forma (Xn,Yn) - TABELA');
    fprintf('(\t  Xn\t  |\t\t  Yn    )\n');
    for i=1:N+1
        fprintf('(%.10f | %.10f)\n',resultado(i,1),resultado(i,2));
    end
    plot(resultado(:,1),resultado(:,2),'*r')
    grid on