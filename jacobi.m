%Jacobi-Richardson
function jacobi(A,b,e)
    [m,n]=size(A); %[linhas,colunas]=size(A)
    if m ~= n %diferente: ~=
        disp('A matriz deve ser quadrada');
        return
    end
    disp('A=')
    disp(A)
    disp('b=')
    disp(b)
    %Verificando elementos da diagonal principal
    for i=1:n
        if abs(A(i,i))<10^(-10)
            fprintf('Elemento A(%d,%d)=0',i,i)
            disp('Elemento da diagonal principal não pode ser zero!')
        end
    end
    %Matriz A* e b*
 b=b./diag(A);      %diag(A): Vetor com os elementos da diagonal de A
 A=A./diag(A);
 disp('A*=')
 disp(A)
 disp('b=')
 disp(b)
 %analisando o critério de convergência
 %EDD
 NC=false; %Variável lógica para controlar a norma coluna
 for i=1:n
     S=sum(abs(A(i,1:i-1)))+sum(abs(A(i,i+1:n)));
     if S>=1
         fprintf('Linha %d não é EDD\n',i)
         NC=true;
     end
 end
 if NC
     disp('Testando a Norma Coluna')
     A=A';%Transposta da matriz A*
    for i=1:n
     S=sum(abs(A(i,1:i-1)))+sum(abs(A(i,i+1:n)));
      if S>=1
         fprintf('Coluna %d não atende a Norma Coluna\n',i)
         disp('Convergência não garantida!')
         return
      end
    end
    A=A';
    disp('Norma coluna verificada. Convergência garantida!')
 else
      disp('A matriz é EDD. Convergência garantida!') 
 end
 %Método de Jacobi-Richardson
 k=0; %contador de iteração
 erro=100;
 x=b;
 disp('x(0)')
 disp(x)
 while k<1000 && erro>e
    xant=x;     % x anterior recebe o x atual
    for i=1:n
        x(i)=b(i)-A(i,1:i-1)*xant(1:i-1)-A(i,i+1:n)*xant(i+1:n);
    end
     erro=max(abs(x-xant))/max(abs(x));
     k=k+1;
     fprintf('x(%d)=\n',k)
     disp(x)
     fprintf('Erro (%d)=%f\n',k,erro)
 end 