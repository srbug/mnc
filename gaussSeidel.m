% #########################################################################
% Exemplos de matrizes para teste (elmGauss(A,b))
%   exemplo 1 - aula
%   A = [5 1 1;3 4 1;3 3 6]; b = [5;6;0]; e = 10^-2;
%   exemplo 2 - TDE
%   A=[5 1 0 3;2 5 -1 1;3 -1 -7 2;-1 2 3 10]; b=[1;-5;-5;-16]; e = 10^-2;
%   exemplo 3 - aleatorio
%   A = [3 1 -1;1 4 1;1 2 -5]; b = [3;6;-6]; e = 10^-2;
% #########################################################################
function gaussSeidel(A,b,e)
    [m,n]=size(A); %[linhas,colunas]=size(A)
    if m ~= n %diferente: ~=
        disp('A matriz deve ser quadrada');
        return
    end
    disp('A=')
    disp(A)
    disp('b=')
    disp(b)
    %Verificando elementos da diagonal principal
    for i=1:n
        if abs(A(i,i))<10^(-10)
            fprintf('Elemento A(%d,%d)=0',i,i)
            disp('Elemento da diagonal principal não pode ser zero!')
        end
    end
    %Matriz A* e b*
 b=b./diag(A);      %diag(A): Vetor com os elementos da diagonal de A
 A=A./diag(A);
 disp('A*=')
 disp(A)
 disp('b=')
 disp(b)
 %analisando o critério de convergência
 %Criterio das linhas (aplicado em A*) <==> EDD (aplicado em A) ; % alterado
 CS=false; %Variável lógica para controlar o criterio de Sassenfeld
 for i=1:n
     S=sum(abs(A(i,1:i-1)))+sum(abs(A(i,i+1:n))); % alterado
     if S>=1
         fprintf('Linha %d não é EDD\n',i)
         CS=true;
         break;  % alterado
     end
 end
 if CS
    disp('Testando o criterio de Sassenfeld');  % alterado
    beta(n) = 0; 
    for i=1:n
        beta(i)=sum(abs(beta(1:i-1).*A(i,1:i-1)))+sum(abs(A(i,i+1:n)));
        if max(beta)>=1  % alterado
            fprintf('Linha %d não atende o criterio de Sassenfeld\n',i);
            disp('Convergência não garantida!');
            return;
        end
    end
    disp('beta=');
    disp(beta);
    disp('Criterio de Sassenfeld verificado. Convergência garantida!')
 else
      disp('A matriz é EDD. Convergência garantida!') 
 end
 %Método de Gauss-Seidel
 k=0; %contador de iteração
 erro=100;
 x(n,1)=0; % alterado
 disp('x(0)')
 disp(x)
 while k<1000 && erro>e
    xant=x;     % x anterior recebe o x atual
    for i=1:n
        x(i)=b(i)-A(i,1:i-1)*x(1:i-1)-A(i,i+1:n)*xant(i+1:n);  % alterado
    end
     erro=max(abs(x-xant))/max(abs(x));
     k=k+1;
     fprintf('x(%d)=\n',k)
     disp(x)
     fprintf('Erro (%d)=%f\n',k,erro)
 end 
    