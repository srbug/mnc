%Método das Newton

% exemplo 1
% f(x)= 4*cos(x)-exp(x)
% x0 = 1
% e = 0.01
% n= 10

% exemplo 2
% f(x)= exp(x)-x-2
% x0= -1.5
% e = 0.01
% iterações n= 1

% exemplo 3 - TDE 5
% f(x)= -50+x*(cosh(500/(2*x))-1)
% x0= -30
% e = 10^-10
% iterações n=50

function newton()  
    x = sym('x');
    f = input('insira a função f(x)= ');
    xk = input('Insira o valor inicial x0= ');
    e = input('Insira a precisão e= ');
    n = input('Insira o número máximo de iterações n= ');
    
    fd = diff(f);
    disp(fd)
    k=0;
    
    while k <= n
        k = k+1;
        if subs(fd,xk) == 0
            disp('Derivada igual a zero, escolha outro valor inicial');
            return 
        end
        %disp(subs(f,xk))
        xk1 = eval(xk-subs(f,xk)/subs(fd,xk));
        erro = abs(xk1-xk)/max([1,abs(xk1)]);
        fprintf('x(%d)=%f ; erro = %f\n', k, xk1, erro);
        if erro < e
             fprintf('A raiz é x(%d)=%f ; erro = %f\n', k, xk1, erro);
             return
        end
        xk = xk1;
    end