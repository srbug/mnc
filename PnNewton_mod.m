% #########################################################################
% https://pt.symbolab.com/solver/simplify-calculator/
% PnNewton_mod(x,y);
%   exemplo 1 - aula
%   x = [-1 0 3]; y = [15 8 -1];
%   exemplo 2 - TDE
%   x = [1 2 3]; y = [1.5708 1.5719 1.5739];
%   exemplo 2 - TDE
%   x = [1 2 3]; y = [1.5708 1.5719 1.5739];
% #########################################################################

%Polinômio Interpolador de Newton
function PnNewton_mod(t,f)
    x=sym('x');
    if length(t)~=length(f)
        disp('Quantidade de valores em x e y não confere!')
        return
    end
    n=length(t);
    fxi = "";
    countX = 0;
    % TABELA - START ---
    fprintf('TABELA - START ---\n');
    %Xi
    fprintf('Xi\n');
    for idx = 1:length(t) 
        fprintf("%f\n",t(idx));
    end
    fprintf('####\n');
    
    %f[x0]
    fprintf('f[x0]\n');
    for idx = 1:length(f) 
        fprintf("%f\n",f(idx));
    end
    fprintf('####\n');
    
    %Diferenças Divididas
    DifDiv=f;
    for k=1:n-1
       l = strings(1,(n-k));
       countList = (n-k);
       for i=n:-1:k+1
           fxi = fxi + sprintf('x%d,',countX);
           str = sprintf('(%f-%f)/(%f-%f)=',DifDiv(i),DifDiv(i-1),t(i),t(i-k));
           DifDiv(i)=(DifDiv(i)-DifDiv(i-1))/(t(i)-t(i-k));
           l(countList) = str + string(DifDiv(i)) + "\n";
           countList = countList-1;
           countX = countX + 1;
       end
       fprintf('f[%s]\n',extractBefore(fxi, length(char(fxi))));
       for idx = 1:length(l) 
           fprintf(l(idx));
       end
       fprintf('####\n');
    end
    fprintf('TABELA - END ---\n\n');
    
    % P2(x) - START ---
    fprintf('P2(x)=');
    for i=1:n
        for k=1:i-1
            fprintf('(x-x%d)',(k-1));
        end
        fprintf('f[');
        fxi = "";
        for j=0:i-1
            fxi = fxi + sprintf('x%d,',j);
        end
        if i == n
            fprintf('%s]',extractBefore(fxi, length(char(fxi))));
        else
            fprintf('%s]+',extractBefore(fxi, length(char(fxi))));
        end
    end
    fprintf('\n\n');
    % P2(x) - END ---

    disp('Diferenças divididas utilizadas no polinômio');
    disp('da maior para a menor ordem');
    disp(DifDiv)
    
    %Pn
    Pn=DifDiv(1);
    for i=2:n
        Prod=1;
        for j=1:i-1
            Prod=Prod*(x-t(j));
        end
       Pn=Pn+Prod*DifDiv(i); 
    end
    Pn=simplify(Pn);
    fprintf('P_%d(x)=%s\n' ,n-1,Pn);