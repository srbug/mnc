% #########################################################################
% https://pt.symbolab.com/solver/simplify-calculator/
% PnNewton(x,y);
%   exemplo 1 - aula
%   x = [-1 0 3]; y = [15 8 -1];
%   exemplo 2 - TDE
%   x = [1 2 3]; y = [1.5708 1.5719 1.5739];
% #########################################################################

%Polinômio Interpolador de Newton
function PnNewton(t,f)
    x=sym('x')
    if length(t)~=length(f)
        disp('Quantidade de valores em x e y não confere!')
        return
    end
    n=length(t);
    %Diferenças Divididas
    DifDiv=f;
    for k=1:n-1
       for i=n:-1:k+1
           DifDiv(i)=(DifDiv(i)-DifDiv(i-1))/(t(i)-t(i-k));
       end
    end
    disp('Diferenças divididas utilizadas no polinômio')
    disp('da maior para a menor ordem')
    disp(DifDiv)
    %Pn
    Pn=DifDiv(1);
    for i=2:n
        Prod=1;
        for j=1:i-1
            Prod=Prod*(x-t(j));
        end
       Pn=Pn+Prod*DifDiv(i); 
    end
    Pn=simplify(Pn);
    fprintf('P_%d(x)=%s\n' ,n-1,Pn)
    %Gráfico
    eixox=linspace(t(1)-1,t(n)+1); %   linspace(a,b): gera 100 valores entre a e b
    eixoy=subs(Pn,eixox);
    plot(t,f,'b*',eixox,eixoy,'k')        %COR FORMATO TIPO_DE_LINHA
    grid on
    title('Pn(x)')
    xlabel('x')
    ylabel('y')