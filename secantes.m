%Método das Secantes
function secantes()
x=sym('x');
f=input('insira f(x) = ');
xk_1=input('insira x0 = ');
xk=input('insira x1 = ');
e=input('insira precisão e = ');
n=input('insira o número máximo de iterações n = ');
k=1;
%fprintf('x(0) = %.30f\n',xk_1)
%fprintf('x(1) = %.30f\n',xk)
while k<=n
    k=k+1;
    xk1=eval((xk_1*subs(f,xk)-xk*subs(f,xk_1))/(subs(f,xk)-subs(f,xk_1)));   %Método das Secantes
    erro= abs(xk1-xk)/max([1,abs(xk1)]);
    if  erro<e
       fprintf('A raiz é x(%d) = %.30f ; erro = %.30f\n',k,xk1,erro)
       return
    else 
        fprintf('A raiz é x(%d) = %.30f ; erro = %.30f\n',k,xk1,erro)
    end
    xk_1=xk;
    xk=xk1;
end