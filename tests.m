%int(1/sqrt(1 - k^2 * sin(x)^2), x, 0, pi/2)
function tests()
    syms k x;
    K = input('K(k)= ');
    for k=1:3
        disp(k)
        disp(eval(subs(K,k)))
        fprintf('----\n')
    end
    fprintf('\n');