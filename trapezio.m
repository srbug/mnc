% #########################################################################
% Exemplos de teste -> trapezio(f,a,b,n)
%   DICA: n = b/h
%   
%   exemplo 27 - aula
%       - syms('x');
%       - f=exp(x)*cos(x); -> f(x)
%       - a=0; -> limite inferior
%       - b=1.2; -> limite superior
%       - n=6; -> numero de sub intervalos
%   exemplo - TDE
%       - syms('x');
%       - f=sqrt(1+((8*x)-3)^2) -> f(x)
%       - a=0; -> limite inferior
%       - b=1; -> limite superior
%       - n=2|n=4|n=10; -> numero de sub intervalos
% #########################################################################

%Regra do Trapezio
function [I]=trapezio(f,a,b,n)
    fprintf('Sera calculado pela regra do trapezio\n');
    h=(b-a)/n;
    fprintf('\nParametros de entrada--\n');
    fprintf('a=%f\n', a);
    fprintf('b=%f\n', b);
    fprintf('f(x)=%s\n', f);
    fprintf('numero de subintervalos=%f\n', n);
    fprintf('h=%f\n', h);
    fprintf('\n\n');
    I=eval(subs(f,a))+eval(subs(f,b)); %f(0)+f(n) soma das 2 pontas
    for i=1:n-1
        I=I+2*eval(subs(f,(a+(i*h))));
    end
    I=I*h/2;
    fprintf('A aproximacao da integral de %s\n',f);
    fprintf('de %.4f ate %.4f com %d subintervalos é\n%.20f\n',a,b,n,I);
    